<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
})->name('welcome');

// Route::get('/home', function () {
//     return view('front.index');
// })->name('welcome');
route::prefix('admin')->middleware('checkrole')->group(function(){
    Route::get('/', 'back\AdminController@index')->name('admin.index');
    Route::get('/users', 'back\UserController@index')->name('admin.users');
    Route::get('/profile/{user}','back\UserController@edit')->name('admin.user.profile');
    Route::post('/updateuser/{user}','back\UserController@update')->name('admin.user.update');
    Route::get('/deleteuser/{user}','back\UserController@destroy')->name('admin.user.delete');
    Route::get('/updatestatus/{user}','back\UserController@updatestatus')->name('admin.user.status');
});

route::prefix('admin/categories')->middleware('checkrole')->group(function(){
    Route::get('/', 'back\categoryController@index')->name('admin.categories');
    Route::get('/create', 'back\categoryController@create')->name('admin.categories.create');
    Route::get('/{category}', 'back\categoryController@show')->name('admin.categories.show');
    Route::post('/store', 'back\categoryController@store')->name('admin.categories.store');
    Route::get('/edit/{category}','back\categoryController@edit')->name('admin.categories.edit');
    Route::post('/update/{category}','back\categoryController@update')->name('admin.categories.update');
    Route::get('/delete/{category}','back\categoryController@destroy')->name('admin.categories.delete');
});


route::prefix('admin/articles')->middleware('checkrole')->group(function(){
    Route::get('/', 'back\articleController@index')->name('admin.articles');
    Route::get('/create', 'back\articleController@create')->name('admin.articles.create');
    Route::get('/{category}', 'back\articleController@show')->name('admin.articles.show');
    Route::post('/store', 'back\articleController@store')->name('admin.articles.store');
    Route::get('/edit/{article}','back\articleController@edit')->name('admin.articles.edit');
    Route::post('/update/{article}','back\articleController@update')->name('admin.articles.update');
    Route::get('/delete/{article}','back\articleController@destroy')->name('admin.articles.delete');
    Route::get('/updatestatus/{article}','back\articleController@updatestatus')->name('admin.articles.status');

});

route::prefix('admin/comments')->middleware('checkrole')->group(function(){
    Route::get('/', 'back\commentController@index')->name('admin.comments');
    Route::get('/edit/{comment}','back\commentController@edit')->name('admin.comments.edit');
    Route::post('/update/{comment}','back\commentController@update')->name('admin.comments.update');
    Route::get('/delete/{comment}','back\commentController@destroy')->name('admin.comments.delete');
    Route::get('/updatestatus/{comment}','back\commentController@updatestatus')->name('admin.comments.status');
});




Route::get('/profile/{user}','UserController@edit')->name('profile')->middleware(['auth','verified']);
Route::post('/update/{user}','UserController@update')->name('profile.update');
Route::get('/articles','front\articleController@index')->name('articles');
Route::get('/article/{article}','front\articleController@show')->name('article');
Route::post('/comment/{article}', 'front\commentController@store')->name('comment.store');



Auth::routes(['verify'=>true]);

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
