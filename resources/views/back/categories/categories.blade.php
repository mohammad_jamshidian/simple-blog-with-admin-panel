@extends('back.index')

@section('page_title', 'پنل مدیریت _ دسته بندی ها')

@section('content')
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header text-right">
                        <h4 class="page-title">لیست دسته بندی ها</h4>
                        <div class="float-left w-75">
                            <a href="{{ route('admin.categories.create') }}" class="btn btn-primary float-left px-3">دسته بندی
                                جدید </a>
    
                            </div>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> لیست دسته بندی ها </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <table class="table table-striped overflow-auto text-center">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col"> ایدی </th>
                                        <th scope="col"> نام دسته بندی </th>
                                        <th scope="col"> اسلاگ (slug) </th>
                                        <th scope="col">مدیریت</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)

                                        <tr>
                                            <th scope="row">
                                                {{ $category->id }}
                                            </th>
                                            <td>
                                                {{ $category->name }}
                                            </td>
                                            <td>
                                                {{ $category->slug }}
                                            </td>

                                            <td>
                                                <a href="{{ route('admin.categories.show', $category->id) }}"
                                                    class="badge badge-success">مشاهده</a>
                                                <a href="{{ route('admin.categories.edit', $category->id) }}"
                                                    class="badge badge-success">ویرایش</a>
                                                <a href="{{ route('admin.categories.delete', $category->id) }}"
                                                    class="badge badge-warning"
                                                    onclick="return confirm('ایا از حذف کاربر مطمئن هستید ؟');">
                                                        حذف
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mx-auto">
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
