@extends('back.index')

@section('page_title', 'پنل مدیریت _ ویرایش دسته بندی')

@section('content')\
    {{-- {{ dd($errors->all()) }} --}}
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if ($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header text-right">
                        <h4 class="page-title text-right">ویرایش دسته بندی</h4>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.categories') }}"> لیست دسته بندی ها </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> ویرایش دسته بندی </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <form dir="rtl" action="{{ route('admin.categories.update', $category->id) }}" method="POST"
                                class="rounded text-right p-4 flex-fill">
                                @csrf
                                <div class="form-group ">
                                    <label for="fullname">نام :</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        id="fullname" value="{{ $category->name }}" name="name">
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="email">اسلاگ (slug) :</label>
                                    <input type="text" class="form-control @error('slug') is-invalid @enderror" id="slug"
                                        value="{{ $category->slug }}" name="slug">
                                </div>

                                <button type="submit" class="btn btn-primary btn-lg btn-block">ویرایش</button>
                                <a href="{{ route('admin.categories') }}"
                                    class="btn btn-warning float-left btn-lg btn-block">انصراف</a>
                            </form>

                        </div>
                        <div class="mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
