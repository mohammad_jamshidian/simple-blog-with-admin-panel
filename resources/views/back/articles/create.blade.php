@extends('back.index')

@section('page_title', 'پنل مدیریت _ مطلب جدید')

@section('content')
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if ($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header">
                        <h4 class="page-title"> ساخت مطلب جدید </h4>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.articles') }}"> لیست مطالب </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> مطلب جدید </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <form dir="rtl" action="{{ route('admin.articles.store') }}" method="POST"
                                class="rounded text-right p-4 flex-fill">
                                @csrf
                                <div class="form-group ">
                                    <label for="title">تیتر :</label>
                                    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                        placeholder="تیتر مطلب را وارد کنید . . ." name="title" value="{{ old('title') }}">
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="email">اسلاگ (slug) :</label>
                                    <input type="text" class="form-control @error('slug') is-invalid @enderror" id="slug"
                                        placeholder="اسلاگ (slug) مطلب را وارد نمایید . . ." name="slug"
                                        value="{{ old('title') }}">
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="description">محتوای مطلب :</label>
                                    <textarea type="text" class="form-control @error('description') is-invalid @enderror"
                                        id="editor" placeholder="محتوای مطلب خود را وارد نمایید . . ."
                                        name="description">
                                    {{ old('description') }}
                                    </textarea>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="status">وضعیت :</label>
                                    <select type="text" class="form-control" id="status" name="status">
                                        <option value="0">منتشر نشده</option>
                                        <option value="1">منتشر شده</option>
                                    </select>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="categories">دسته بندی :</label>
                                    <div id="output"></div>
                                    <select class="chosen-select" id="categories" name="categories[]" multiple="multiple"
                                        style="width: 400px" data-placeholder="دسته بندی مطلب خود را انتخاب کنید . . .">
                                        @foreach ($categories as $cat_id => $cat_name)
                                            <option value="{{ $cat_id }}">{{ $cat_name }}</option>
                                        @endforeach
                                    </select>
                                </div>






                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="user_id">نویسنده :
                                        {{ Auth::user()->name }}</label>
                                    <input type="hidden" id="slug" name="user_id" value="{{ Auth::user()->id }}">
                                </div>

                                <button type="submit" class="btn btn-primary btn-lg btn-block">ثبت</button>
                                <a href="{{ route('admin.articles') }}"
                                    class="btn btn-warning float-left btn-lg btn-block">انصراف</a>
                            </form>
                        </div>
                        <div class="mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
