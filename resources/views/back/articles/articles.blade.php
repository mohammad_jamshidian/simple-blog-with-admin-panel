@extends('back.index')

@section('page_title', 'پنل مدیریت _ مدیریت مطالب')

@section('content')
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if ($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header text-right">
                        <h4 class="page-title">لیست مطالب </h4>
                        <div class="float-left w-75">
                            <a href="{{ route('admin.articles.create') }}" class="btn btn-primary float-left px-3">مطلب
                                جدید </a>
                        </div>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> لیست مطالب </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <table class="table table-responsive table-striped overflow-auto text-center">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col"> نام </th>
                                        <th scope="col"> اسلاگ (slug) </th>
                                        <th scope="col">دسته بندی</th>
                                        <th scope="col">نویسنده</th>
                                        <th scope="col">تعداد بازدید</th>
                                        <th scope="col">وضعیت</th>
                                        <th scope="col">مدیریت</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($articles as $article)
                                        @switch($article->status)
                                            @case(1)
                                            @php
                                            $url = route('admin.articles.status',$article->id);
                                            $status = '<a href="' . $url . '" class="btn btn-success m-auto "></a>';
                                            @endphp
                                            @break

                                            @case(0)
                                            @php
                                            $url = route('admin.articles.status',$article->id);
                                            $status = '<a href="' . $url . '" class="btn btn-danger m-auto "></a>';
                                            @endphp
                                            @break

                                            @default

                                        @endswitch

                                        <tr>
                                            <td>
                                                {{ $article->title }}
                                            </td>

                                            <td>
                                                {{ $article->slug }}
                                            </td>

                                            <td class="d-flex flex-wrap h-auto border-0">
                                                    @foreach ($article->categories()->pluck('name') as $category)
                                                        <span class="badge badge-warning  ">{{$category}}</span>
                                                    @endforeach
                                            </td>

                                            <td>
                                                {{ $article->user->name }}
                                            </td>

                                            <td>
                                                {{ $article->hits }}
                                            </td>
                                            <td>
                                                {!!$status!!}
                                            </td>

                                            <td class="d-flex flex-wrap h-auto border-0">
                                                <a href="{{ route('admin.articles.show', $article->id) }}"
                                                    class="badge badge-success">مشاهده</a>
                                                <a href="{{ route('admin.articles.edit', $article->id) }}"
                                                    class="badge badge-success">ویرایش</a>
                                                <a href="{{ route('admin.articles.delete', $article->id) }}"
                                                    class="badge badge-warning"
                                                    onclick="return confirm('ایا از حذف کاربر مطمئن هستید ؟');">
                                                        حذف
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mx-auto">
                            {{ $articles->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
