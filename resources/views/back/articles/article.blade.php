@extends('back.index')

@section('page_title', 'پنل مدیریت _ دسته بندی ها')

@section('content')
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header">
                        <h4 class="page-title">لیست دسته بندی ها</h4>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.categories') }}"> لیست دسته بندی ها </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> نمایش دسته بندی </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-md-8 col-sm-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <table class="table border overflow-auto text-center">
                                <tbody>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <td>
                                            {{ $category->name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            اسلاگ (slug)
                                        </th>
                                        <td>
                                            {{ $category->slug }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            زمان ساخت
                                        </th>
                                        <td>
                                            {{ $category->created_at }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            زمان اپدیت
                                        </th>
                                        <td>
                                            {{ $category->updated_at }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            مدیریت
                                        </th>
                                        <td>
                                            <!-- Single edit button -->
                                            <a href="{{ route('admin.categories.edit', $category->id) }}"
                                                class="btn text-success btn-link">
                                                <i class="la la-edit"></i>
                                                ویرایش
                                            </a>


                                            <a href="{{ route('admin.categories.delete', $category->id) }}"
                                                class="btn text-danger btn-link">
                                                <i class="la la-trash"></i>
                                                حذف
                                            </a>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                            <a href="{{ route('admin.categories') }}" class="btn btn-outline-danger mt-2 btn-block"> بازگشت </a>
                        </div>
                        <div class="mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
