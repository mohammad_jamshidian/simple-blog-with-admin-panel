@extends('back.index')

@section('page_title', 'پنل مدیریت _ ویرایش کامنت')

@section('content')
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if ($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header">
                        <h4 class="page-title"> ویرایش مطلب </h4>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.comments') }}"> لیست مطالب </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> ویرایش مطلب </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <form dir="rtl" action="{{ route('admin.comments.update', $comment->id) }}" method="POST"
                                class="rounded text-right p-4 flex-fill">
                                @csrf
                                <div class="form-group">
                                    <label for="user_name">نام کاربر</label>
                                    <input id="user_name" class="form-control @error('user_name') is-invalid @enderror"
                                        type="text" placeholder="نام کاربر را وارد نمایید . . ." name="user_name"
                                value="{{$comment->user_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="user_email">ایمیل کاربر</label>
                                    <input id="user_email" class="form-control @error('user_email') is-invalid @enderror"
                                        type="text" placeholder="ایمیل کاربر را وارد نمایید . . ." name="user_email"
                                value="{{$comment->user_email}}">
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="body">متن کامنت </label>
                                    <textarea class="form-control @error('body') is-invalid @enderror"
                                        id=""name="body">
                                    {{ $comment->body }}
                                    </textarea>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="status">وضعیت :</label>
                                    <select type="text" class="form-control @error('status') is-invalid @enderror"
                                        id="status" name="status">
                                        <option value="0" @if ($comment->status == 0) selected
                                            @endif>منتشر نشده</option>
                                        <option value="1" @if ($comment->status == 1) selected
                                            @endif>منتشر شده</option>
                                    </select>
                                </div>
                                {{-- <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="user_id">نویسنده :
                                        {{ Auth::user()->name }}</label>
                                    <input type="hidden" id="slug" name="user_id" value="{{ Auth::user()->id }}">
                                </div> --}}

                                <button type="submit" class="btn btn-primary btn-lg btn-block">ویرایش</button>
                                <a href="{{ route('admin.comments') }}"
                                    class="btn btn-warning float-left btn-lg btn-block">انصراف</a>
                            </form>
                        </div>
                        <div class="mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
