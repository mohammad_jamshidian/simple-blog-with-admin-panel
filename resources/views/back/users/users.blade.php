@extends('back.index')

@section('page_title', 'پنل مدیریت _ مدیریت کاربران')

@section('content')
    <div class="main-panel">
        <div class="w-25 h-auto fixed-top-left d-flex align-content-top flex-wrap mt-5 pb-3 px-2">
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header">
                        <h4 class="page-title">لیست کاربران</h4>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> لیست کاربران </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped text-center">
                                <thead>
                                    <tr>
                                        <th> ایدی </th>
                                        <th> نام </th>
                                        <th> ایمیل </th>
                                        <th> تلفن همراه </th>
                                        <th> نوع عضویت </th>
                                        <th> وضعیت </th>
                                        <th> مدیریت </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        @switch($user->role)
                                            @case(1)
                                            @php
                                            $role = 'مدیر';
                                            @endphp
                                            @break
                                            @case(2)
                                            @php
                                            $role = 'کاربر';
                                            @endphp
                                            @break
                                            @default

                                        @endswitch
                                        @switch($user->status)
                                            @case(1)
                                            @php
                                            $url = route('admin.user.status',$user->id);
                                            $status = '<a href="' . $url . '" class="btn btn-success m-auto " ></a>';
                                                @endphp
                                                @break

                                                @case(0)
                                                @php
                                                $url = route('admin.user.status',$user->id);
                                                $status = '<a href="' . $url . '" class="btn btn-danger m-auto "></a>';
                                                    @endphp
                                                    @break

                                                    @default

                                                @endswitch
                                                <tr>
                                                    <td>
                                                        {{ $user->id }}
                                                    </td>
                                                    <td>
                                                        {{ $user->name }}
                                                    </td>
                                                    <td>
                                                        {{ $user->email }}
                                                    </td>
                                                    <td>
                                                        {{ $user->phone }}
                                                    </td>
                                                    <td>
                                                        {{ $role }}
                                                    </td>
                                                    <td>
                                                        {!! $status !!}
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.user.profile', $user->id) }}"
                                                            class="badge badge-success">ویرایش</a>
                                                        <a href="{{ route('admin.user.delete', $user->id) }}"
                                                            class="badge badge-warning"
                                                            onclick="return confirm('ایا از حذف کاربر مطمئن هستید ؟');">
                                                                حذف
                                                        </a>
                                                    </td>
                                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mx-auto">
                            {{$users->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('back.footer')
    </div>
@endsection
