@extends('back.index')

@section('page_title', 'پنل مدیریت _ ویرایش کاربر')


@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row  mb-0 page-title-header">
                <div class="col-12">
                    <div class="page-header text-right">
                        <h4 class="page-title">ویرایش اطلاعات کاربری</h4>
                    </div>
                </div>
            </div>
            <nav aria-label="breadcrumb pt-0">
                <ol class="breadcrumb border-bottom  px-5">
                    <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"> پنل مدیریت </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.users') }}"> لیست کاربران </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> ویرایش اطلاعات کاربری </li>
                </ol>
            </nav>
            <!-- Page Title Header Ends-->
            <div class="row">

                <div class="col-12 grid-margin stretch-card">

                    <div class="card">
                        <div class="card-body">
                            <div class="w-25 h-auto fixed-top d-flex align-content-top flex-wrap px-2">
                                @if($errors->any())
                                    {{-- {{ dd($errors) }} --}}
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-danger m-1 alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $error }}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <form dir="rtl" action="{{ route('admin.user.update', $user->id) }}" method="POST"
                                class="rounded text-right p-4 flex-fill">
                                @csrf
                                <div class="form-group ">
                                    <label for="fullname">نام :</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        id="fullname" value="{{ $user->name }}" name="name">
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-form-label" for="email">ایمیل :</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                        value="{{ $user->email }}" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="phone">شماره موبایل</label>
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror" id="pwd"
                                        value="{{ $user->phone }}" name="phone">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">ویرایش رمز عبور :</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="pwd" placeholder="رمز کاربر را اینجا وارد کنید . . ." name="password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">تکرار رمز عبور :</label>
                                    <input type="password"
                                        class="form-control @error('password_confirmation') is-invalid @enderror" id="pwd"
                                        placeholder="رمز را مجددا وارد نمایید . . ." name="password_confirmation">
                                </div>
                                <div class="form-group">
                                    <label for="status">وضعیت کاربر :</label>
                                    <select id="status" name="status" class="form-control">
                                        <option value="1" @if($user->status == 1) selected @endif>فعال</option>
                                        <option value="0" @if($user->status == 0) selected @endif>غیر فعال</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="role">نوع عضویت کاربر :</label>
                                    <select id="role" name="role" class="form-control">
                                        <option value="1" @if($user->role == 1) selected @endif>مدیر</option>
                                        <option value="2" @if($user->role == 2) selected @endif>کاربر</option>
                                    </select>

                                </div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">ویرایش</button>
                            <a href="{{route('admin.users')}}" class="btn btn-warning float-left btn-lg btn-block">انصراف</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    @include('back.footer')
    </div>

@endsection
