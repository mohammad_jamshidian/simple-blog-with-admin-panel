<!DOCTYPE html>
<html lang="en">

<head>
    {!! htmlScriptTagJsApi(['lang'=>'fa']) !!}

    <meta charset="utf-8">
    <title>@yield('page_title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="front/img/favicon.png" rel="icon">
    <link href="front/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700"
        rel="stylesheet">

    <link href="{{ url('front/app.css') }}" rel="stylesheet">
    <link href="{{ url('front/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ url('front/ionicons/css/ionicons.min.css') }}" rel="stylesheet">




    <!-- =======================================================
    Theme Name: Rapid
    Theme URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body dir="rtl" class="text-right">

    @include('front.layouts.navbar')

    @yield('content')

    @include('front.layouts.footer')

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->

    <!-- JavaScript Library -->
    <script src="{{ url('front/app.js') }}"></script>

</body>

</html>
