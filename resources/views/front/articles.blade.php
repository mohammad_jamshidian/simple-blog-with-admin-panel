@extends('front.layouts.layout')

@section('page_title', 'مطالب')

@section('content')
    <style>
        .immg {
            height: 300px;
            width: 300px;
        }

    </style>
    <section id="intro2" class="clearfix ">
        <div class="container d-flex ">
        </div>
    </section>
    <main id="main d-flex align-items-center">
        <div class="w-25 h-auto fixed-top d-flex align-content-top flex-wrap px-2">
        </div>
        <div class=" m-auto w-100">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb border-bottom m-5 px-5">
                    <li class="breadcrumb-item"><a href="{{ route('welcome') }}"> خانه </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> ثبت نام </li>
                </ol>
            </nav>
            <div class="container-flude ">
                <div class="row m-0 p-2 justify-content-center">
                    
                    @foreach ($articles as $article)
                        <div class="col-auto mb-3">
                            <div class="card" style="width: 17rem;">
                                <img src="photos/img.jpg" class="card-img-top" alt="">
                                <div class="card-body ">
                                    <h5 class="card-title">
                                        <a href="{{ route('article', $article->slug) }}">{{ $article->title }}</a>
                                    </h5>
                                    <ul>
                                        <li  class="card-text m-0">{{$article->user->name}}</li>
                                        <li  class="card-text m-0">{!! \ Morilog\Jalali\Jalalian::forge($article->created_at)->ago() !!}</li>
                                        {{-- <li  class="card-text m-0">{!! jdate($article->created_at)->format('%d-%m-%y') !!}</li> --}}
                                        <li  class="card-text mb-2"> بازدید {{$article->hits}}</li>
                                    </ul>

                                    <p class="card-text">
                                        <?php echo mb_substr(strip_tags($article->description), 0, 200,'UTF8') .
                                        ' . .
                                        . '; ?>
                                    </p>
                                <a href="{{route('article',$article->slug)}}" class="btn btn-primary"> ادامه مطلب . . . </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
        {{ $articles->links() }}
    </main>
@endsection
