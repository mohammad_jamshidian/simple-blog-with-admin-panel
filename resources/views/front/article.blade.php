@extends('front.layouts.layout')

@section('page_title', $article->title)

@section('content')
    <section id="intro2" class="clearfix ">
        <div class="container d-flex ">
        </div>
    </section>
    <main id="main d-flex align-items-center">
        <div class="w-25 h-auto fixed-top d-flex align-content-top flex-wrap px-2">
            @if ($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if (session('success'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-success m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                {{-- {{ dd($errors) }} --}}
                <div class="alert alert-warning m-1 w-100 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ session('warning') }}
                </div>
            @endif
        </div>
        <div class=" m-auto w-100">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb border-bottom m-5 px-5">
                    <li class="breadcrumb-item"><a href="{{ route('welcome') }}"> خانه </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('articles') }}"> مطالب </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> </li>
                </ol>
            </nav>
            <div class="container">

                <div class="row  d-block">
                    <div class="text-center">
                        <h3><a href="">{{ $article->title }}</a></h3>
                    </div>
                    <div class=" justify-content-center">
                        <img src="{{ url('photos/img.jpg') }}" alt="">
                    </div>
                    <p class="b-4">{!! $article->description !!}</p>
                    <hr>
                    <ul>
                        <li class="card-text m-0"> نویسنده {{ $article->user->name }}</li>
                        <li class="card-text m-0"> تاریخ {!! jdate($article->created_at)->format('%d-%m-%y') !!}</li>
                        <li class="card-text mb-2"> بازدید {{ $article->hits }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <hr />
            <form action="{{ route('comment.store', $article->slug) }}" method="post">
                @csrf
                @auth
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="user_name">نام کاربر </label>
                            <input id="user_name" class="form-control @error('user_name') is-invalid @enderror" type="text"
                                name="user_name" value="{{ auth::user()->name }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="user_email">ایمیل کاربر :</label>
                            <input id="user_email" class="form-control @error('user_email') is-invalid @enderror" type="text"
                                name="user_email" value="{{ auth::user()->email }}" readonly>
                        </div>
                    </div>
                @else
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="user_name">نام کاربر </label>
                            <input id="user_name" class="form-control @error('user_name') is-invalid @enderror" type="text"
                                name="user_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="user_email">ایمیل کاربر :</label>
                            <input id="user_email" class="form-control @error('user_email') is-invalid @enderror" type="text"
                                name="user_email">
                        </div>
                    </div>
                @endauth
                <div class="form-group">
                    <label for="body">متن کامنت :</label>
                    <textarea id="body" class="form-control @error('body') is-invalid @enderror" name="body" cols="30"
                        rows="10">
                            </textarea>
                </div>
                <div class="form-group">
                    <label for="body">تصویر امنیتی : </label>
                    {!! htmlFormSnippet() !!}
                </div>
                <button class="btn btn-primary" type="submit"> ارسال نظر </button>
            </form>
        </div>
        <hr />

        <div class="container bootstrap snippet">
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-comment">
                        <h3 class="text-success">نظرات</h3>
                        <hr />
                        <ul class="comments" style="list-style: none">
                            @foreach ($comments as $comment)
                                <li class="clearfix bg-light p-3 mb-3">
                                    <div class="post-comments ">
                                        <p class="meta">
                                            {!! \Morilog\Jalali\Jalalian::forge($comment->created_at)->ago() !!}
                                            |
                                            {{ $comment->user_name }}
                                            :
                                        </p>
                                        <p>
                                            {{ $comment->body }}
                                        </p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
