@extends('front.layouts.layout')

@section('page_title', 'فرم ورود')

@section('content')
    <section id="intro2" class="clearfix ">
        <div class="container d-flex ">
        </div>
    </section>
    <main id="main d-flex align-items-center">
        <div class="w-25 h-auto fixed-top d-flex align-content-top flex-wrap px-2">
            @if($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
        </div>
        <div class=" m-auto w-100">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb border-bottom m-5 px-5">
                    <li class="breadcrumb-item"><a href="{{ route('welcome') }}"> خانه </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> ثبت نام </li>
                </ol>
            </nav>
            <div class="m-auto col-6">
                <form dir="rtl" action="login" method="POST" class="rounded p-4 flex-fill">
                    @csrf
                    
                    <div class="form-group">
                        <label for="email">ایمیل :</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                            placeholder="ایمیل خود را وارد نمایید . . ." name="email" value="{{ old('email') }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="pwd">رمز عبور :</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="pwd"
                            placeholder="رمز عبور خود را وارد نمایید . . ." name="password">
                    </div>
                   
                    <div class="form-group form-check">
                        <label class="form-check-label">
                            <input class="" type="checkbox" name="remember"> مرا به خطر بسپار
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="body">تصویر امنیتی : </label>
                        {!! htmlFormSnippet() !!}
                    </div>
                    <button type="submit" class="btn btn-primary w-100">ورود</button>
                </form>
            </div>
        </div>
    </main>
@endsection
