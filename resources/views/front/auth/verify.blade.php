@extends('front.layouts.layout')

@section('page_title', 'فعال سازی ایمیل')

@section('content')
    <section id="intro2" class="clearfix ">
        <div class="container d-flex ">
        </div>
    </section>
    <main id="main d-flex align-items-center">
        <div class="w-25 h-auto fixed-top d-flex align-content-top flex-wrap px-2">
            @if ($errors->any())
                {{-- {{ dd($errors) }} --}}
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger m-1 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            @if (session('resent'))
                <div class="alert alert-success m-1 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    ایمیل فعال سازی ایمیل برای شما راسال شد و روی فعال سازی حساب کاربری کلیلک کنید .
                </div> 
            @endif
        </div>
        <div class=" m-auto w-100">

            <div class="m-auto col-6">
                <div class="card mb-5 mt-5">
                    <div class="card-body">
                        <h5 class="card-title">فعال سازی ایمیل</h5>
                        <p class="card-text"> برای تایید حساب کاربری خود باید روی لینکی ک به صورت ایمیل برای شما
                            ارسال میشود را کلیک کنید ...</p>
                        <p class="card-text">برای فعال سازی ایمیل خود روی دکمه زیر کلیک کنید تا ایمیل فعال سازی برای شما
                            ارسال شود . </p>

                    </div>
                    <div class="card-footer">
                        <form method="post" action="{{ route('verification.resend') }}">
                            @csrf
                            <input class="btn btn-primary w-100" type="submit" value="ایمیل را برای من ارسال کن ">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
