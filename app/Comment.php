<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body','user_name','user_email','status'];
    

    public function article(){
        return $this->belongsTo(Article::class);
    }
}
