<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommentSent extends Mailable
{
    use Queueable, SerializesModels;


    public $comment;
    public $article;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($comment , $article)
    {
        $this->comment = $comment;
        $this->article = $article;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@simpleblog.com')
        ->subject('کامنت شما دریافت شد . ')
        ->view('mails.comment')->with([
            'article_title' => $this->article->title ,
            'comment_body' => $this->comment->body ,
            'user_name' => $this->comment->user_name ,
            ]);
    }
}
