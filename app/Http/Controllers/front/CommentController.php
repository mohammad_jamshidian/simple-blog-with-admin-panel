<?php

namespace App\Http\Controllers\front;


use App\Http\Controllers\Controller;
use App\frontmodels\Article;
use App\frontmodels\Category;
use App\frontmodels\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\CommentSent;

class CommentController extends Controller
{
    public function store(Request $request, Article $article)
    {
        $article_validated = $request->validate([
            'user_name' => 'bail|required',
            'user_email' => 'bail|required',
            'body' => 'bail|required',
            recaptchaFieldName() => recaptchaRuleName()
        ]);

        $article->comments()->create($request->all());

        Mail::to($request->user_email)
        ->send(new CommentSent($request , $article));

        return back()->with('success', 'نظر شما با موفقیت ثبت شد و پس از تایید مدیریت نمایش داده می شود . . .');
    }
}
