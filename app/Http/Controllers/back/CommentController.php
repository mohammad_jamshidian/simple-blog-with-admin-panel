<?php

namespace App\Http\Controllers\back;

use Exception;
use App\Article;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    
    public function index()
    {
        $comments = Comment::orderBy('id','DESC')->paginate(10);
        return view('back/comments/comments', compact('comments'));
    }

    public function edit(Comment $comment)
    {
        return view('back/comments/edit' , compact('comment'));
    }

    public function update(Request $request, Comment $comment)
    {
        
        $validated_comment = $request->validate([
            'body' => 'bail|required',
            'user_name' => 'bail|required',
            'user_email' => 'bail|required',
        ]);

        try {
            $comment->update($request->alL());
            // $comment->article()->sync($request->article);
        } catch (Exception $exception) {
            redirect()->back()->with('warning', $exception->getMessage());
        };
        $smg = 'کامنت با موفقیت ویرایش شد :)';
        return redirect(route('admin.comments'))->with('success', $smg);
    }

    public function destroy(Comment $comment)
    {
        try {
            $comment->delete();
        } catch (Exception $exception) {
            redirect(route('admin.comments'))->with('warning', $exception->getMessage());
        };
        $smg = 'کامنت با موفقیت حذف شد :)';
        return redirect()->back()->compact('smg');
    }

    public function updatestatus(Comment $comment)
    {
        if ($comment->status == 0) {
            $comment->status = 1;
        } else {
            $comment->status = 0;
        }
        $comment->save();
        $smg = 'وضعیت بروز رسانی شد . . .';
        return redirect()->back()->with('success', $smg);
    }
}
