<?php

namespace App\Http\Controllers\back;

use App\Category;
use Illuminate\Http\Request;
use App\http\Controllers\controller;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderby('id', 'DESC')->paginate('20');
        return view('back.categories.categories' , compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category_validated = $request->validate([
            'name' => 'bail|required',
            'slug' => 'bail|required|unique:categories|',
        ]);
        $category = new category;
        try {
            $category->create($request->all());
        } catch (Exception $exception) {
            redirect(route('admin.categories.create'))->with('warning', $exception->getMessage());
        };
        $smg = 'دسته بندی با موفقیت اضافه شد  :)';
        return redirect(route('admin.categories'))->with('success', $smg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('back.categories.category' , compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('back.categories.edit' , compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category_validated = $request->validate([
            'name' => 'bail|required',
            // 'slug' => 'bail|required|unique:categories|',
        ]);
        if ($category->slug != $request->slug) {
            $category_validated = $request->validate([
                'slug' => 'bail|required|unique:categories|',
            ]);
        }elseif($category->slug == $request->slug){
            $category_validated = $request->validate([
                'slug' => 'bail|required',
            ]);        }
        try {
            $category->update($request->all());
        } catch (Exception $exception) {
            redirect(route('admin.categories.edit'))->with('warning', $exception->getMessage());
        };
        $smg = 'دسته بندی با موفقیت بروز رسانی شد  :)';
        return redirect(route('admin.categories'))->with('success', $smg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            $category->delete($category);
        } catch (Exception $exception) {
            redirect(route('admin.categories'))->with('warning', $exception->getMessage());
        };
        $smg = 'دسته بندی مورد نظر شما با موفقیت حذف شد  :)';
        return redirect(route('admin.categories'))->with('success', $smg);
    }
}
