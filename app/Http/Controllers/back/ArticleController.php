<?php

namespace App\Http\Controllers\back;

use exception;
use App\Article;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderby('id', 'DESC')->paginate(10);
        return view('back.articles.articles', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->pluck('name', 'id');
        return view('back.articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article_validated = $request->validate([
            'title' => 'bail|required',
            'slug' => 'unique:categories|',
            'description' => 'required',
        ]);

        $article = new article;

        if (empty($request->slug)) {
            $slug = SlugService::createSlug(Article::class, 'slug', $request->title);
        } else {
            $slug = SlugService::createSlug(Article::class, 'slug', $request->slug);
        }
        $request->merge(['slug' => $slug]);
        try {
            $article = $article->create($request->alL());
            $article->categories()->attach($request->categories);
        } catch (Exception $exception) {
            redirect(route('admin.articles.create'))->with('warning', $exception->getMessage());
        };
        $smg = 'مطلب جدید با موفقیت اضافه شد  :)';
        return redirect(route('admin.articles'))->with('success', $smg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Category::all()->pluck('name', 'id');
        return view('back.articles.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $article_validated = $request->validate([
            'title' => 'bail|required',
            'slug' => 'bail|required|unique:categories|',
            'description' => 'required',
        ]);
        
        $slug = SlugService::createSlug(Article::class, 'slug', $request->slug);
        
        $request->merge(['slug' => $slug]);
        try {
            $article->update($request->alL());
            $article->categories()->sync($request->categories);
        } catch (Exception $exception) {
            redirect(route('admin.articles.edit'))->with('warning', $exception->getMessage());
        };
        $smg = 'مطلب جدید با موفقیت اضافه شد  :)';
        return redirect(route('admin.articles'))->with('success', $smg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->back();
    }

    public function updatestatus(Article $article)
    {
        if ($article->status == 0) {
            $article->status = 1;
        } else {
            $article->status = 0;
        }
        $article->save();
        $smg = 'وضعیت بروز رسانی شد . . .';
        return redirect()->back()->with('success', $smg);
    }
}
