<?php

namespace App\Http\Controllers\back;

use Illuminate\Http\Request;
use App\user;
use Exception;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderby('id', 'DESC')->paginate(20);
        return view('back.users.users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        return view('back.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        if (!empty($request->password)) {

            $profile_validated = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'/*, 'unique:users'*/],
                'phone' => ['required', 'numeric', 'min:11'],
                'password' => ['string', 'min:8', 'confirmed'],
                // 'password-confirmation' => ['string', 'min:8'],
            ]);

            $password = Hash::make($request->password);
            $user->password = $password;
        } else {

            $profile_validated = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'/*, 'unique:users'*/],
                'phone' => ['required', 'numeric', 'min:11'],
            ]);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->status = $request->status;
        $user->role = $request->role;


        try {
            // dd($user);
            $user->save();
        } catch (Exception $exception) {
            redirect()->back()->with('warning', $exception->getMessage());
        };
        $smg = 'ویرایش انجام شد . . .';
        return redirect(route('admin.users'))->with('success', $smg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        $user->delete();
        $smg = 'حذف با موفقیت انجام شد . . .';
        return redirect()->back()->with('success', $smg);
    }
    public function updatestatus(user $user)
    {
        if ($user->status == 0) {
            $user->status = 1;
        }else{
            $user->status = 0;
        }
        $user->save();
        $smg = 'بروزرسانی با موفقیت انجام شد . . .';
        return redirect()->back()->with('success', $smg);
    }
}
