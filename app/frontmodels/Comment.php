<?php

namespace App\frontmodels;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body','user_name','user_email'];
    // protected $attribute = ['status' => 0];

}
