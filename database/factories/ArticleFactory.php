<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->title(null),
        'slug' => $faker->slug('3' , 'true'),
        'description' => $faker->sentence(10 , true),
        'user_id' => $faker->numberBetween(1,43),
        'status' => $faker->numberBetween(0,1)
    ];
});